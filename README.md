# Cross-Ride

Project Assessment:
    Cross-Ride is a ride-sharing application developed by a startup company. Cross-Ride allows its users to register as drivers and/or riders. Registered drivers and riders advertise their usual travel schedule on the application.