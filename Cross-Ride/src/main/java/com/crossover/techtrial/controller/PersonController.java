/**
 * 
 */
package com.crossover.techtrial.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.crossover.techtrial.model.Person;
import com.crossover.techtrial.model.Ride;
import com.crossover.techtrial.service.PersonService;
import com.crossover.techtrial.service.RideService;

/**
 * @author adalberto
 *
 */
@RestController
public class PersonController {
	@Autowired
	  PersonService personService;
	
	@Autowired
	  RideService rideService;
	  
	  @PostMapping(path = "/api/person")
	  public ResponseEntity<Person> register(@RequestBody Person p) {
	    return ResponseEntity.ok(personService.save(p));
	  }
	  
	  @GetMapping(path = "/api/person")
	  public ResponseEntity<List<Person>> getAllPersons() {
	    return ResponseEntity.ok(personService.getAll());
	  }
	  
	  @GetMapping(path = "/api/person/{person-id}")
	  public ResponseEntity<Person> getPersonById(@PathVariable(name="person-id", required=true)Long personId) {
	    Person person = personService.findById(personId);
	    if (person != null) {
	      return ResponseEntity.ok(person);
	    }
	    return ResponseEntity.notFound().build();
	  }
	  
	  @GetMapping(path = "/api/person5")
	  public ResponseEntity<List<List<Object>>> getTop5() throws ParseException {
	    //return ResponseEntity.ok(personService.getTop5());
		  
		  List<List<Object>> list = new ArrayList<>();
		  
		  List<Person> persons = personService.getAll();
		  
		  for (int i = 0; i < persons.size(); i++) {
			  
			  List<Object> object = new ArrayList<>();
			  object.add(persons.get(i));
			  
			  long sumDuration = 0;
			  long sumDistances = 0;
			  
			  List<Ride> rides = rideService.findByDriverId(persons.get(i).getId());
			  for (int j = 0; j < rides.size(); j++) {
				//Get the start and end date in Date Objects
				  SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
				  Date startTime = dateFormat.parse(rides.get(j).getStartTime());
				  Date endTime = dateFormat.parse(rides.get(j).getEndTime());
				  long duration = endTime.getTime() - startTime.getTime();
				  long distance = rides.get(j).getDistance();
				  
				  sumDuration += duration;
				  sumDistances += distance;
				  
			}
			  
			  object.add(sumDuration);
			  object.add(sumDistances);
			  list.add(object);
			
		  }
		  
		  List<List<Object>> finalList = new ArrayList<>();
		  
		  
		  for (int i = 1; i < 5; i++) {
			long mayorDuration = 0; 
			int pos = -1;
			for (int j = 0; j < list.size(); j++) {
				if(mayorDuration < (long) list.get(j).get(1)) {
					mayorDuration = (long) list.get(j).get(1);
					pos = j;
				}
			}
			
			finalList.add(list.get(pos));
			list.remove(pos);
		  }
		  
		return ResponseEntity.ok(finalList);  
	  }
}
