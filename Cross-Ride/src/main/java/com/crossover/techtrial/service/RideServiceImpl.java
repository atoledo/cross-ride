/**
 * 
 */
package com.crossover.techtrial.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.crossover.techtrial.dao.IPersonDAO;
import com.crossover.techtrial.dao.IRideDAO;
import com.crossover.techtrial.model.Person;
import com.crossover.techtrial.model.Ride;
import com.crossover.techtrial.repositories.RideRepository;


/**
 * @author adalberto
 *
 */
@Service
public class RideServiceImpl implements RideService {

	@Autowired
	  RideRepository rideRepository;
	
	@Autowired
	 IRideDAO rideDAO;
	
	/* (non-Javadoc)
	 * @see com.crossover.techtrial.service.RideService#save(com.crossover.techtrial.model.Ride)
	 */
	@Override
	public Ride save(Ride ride) {
		return rideRepository.save(ride);
	}

	/* (non-Javadoc)
	 * @see com.crossover.techtrial.service.RideService#findById(java.lang.Long)
	 */
	@Override
	public Ride findById(Long rideId) {
		Optional<Ride> optionalRide = rideRepository.findById(rideId);
	    if (optionalRide.isPresent()) {
	      return optionalRide.get();
	    }else return null;
	}

	@Override
	public List<Ride> findByDriverId(Long driverId) {
		List<Ride> rideList = new ArrayList<>();
		rideDAO.findByDriverId(driverId).forEach(rideList::add);
	    return rideList;
	}

}
