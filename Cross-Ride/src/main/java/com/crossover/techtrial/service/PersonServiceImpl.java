/**
 * 
 */
package com.crossover.techtrial.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.crossover.techtrial.dao.IPersonDAO;
import com.crossover.techtrial.model.Person;
import com.crossover.techtrial.repositories.PersonRepository;

/**
 * @author adalberto
 *
 */
@Service
public class PersonServiceImpl implements PersonService {

	 @Autowired
	  PersonRepository personRepository;
	 
	 @Autowired
	 IPersonDAO personDAO;
	/* (non-Javadoc)
	 * @see com.crossover.techtrial.service.PersonService#getAll()
	 */
	@Override
	public List<Person> getAll() {
		List<Person> personList = new ArrayList<>();
	    personRepository.findAll().forEach(personList::add);
	    return personList;
	}

	/* (non-Javadoc)
	 * @see com.crossover.techtrial.service.PersonService#save(com.crossover.techtrial.model.Person)
	 */
	@Override
	public Person save(Person p) {
		return personRepository.save(p);
	}

	/* (non-Javadoc)
	 * @see com.crossover.techtrial.service.PersonService#findById(java.lang.Long)
	 */
	@Override
	public Person findById(Long personId) {
		Optional<Person> dbPerson = personRepository.findById(personId);
	    return dbPerson.orElse(null);
	}

	/* (non-Javadoc)
	 * @see com.crossover.techtrial.service.PersonService#getTop5()
	 */
	@Override
	public List<Person> getTop5() {
		List<Person> personList = new ArrayList<>();
		personDAO.getTop5().forEach(personList::add);
	    return personList;
	}

}
