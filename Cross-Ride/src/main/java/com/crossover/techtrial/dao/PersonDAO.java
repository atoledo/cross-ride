/**
 * 
 */
package com.crossover.techtrial.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.crossover.techtrial.model.Person;

/**
 * @author adalberto
 *
 */
@Transactional
@Repository
public class PersonDAO implements IPersonDAO {

	@PersistenceContext	
	private EntityManager entityManager;
	
	/**
	 * 
	 */
	public PersonDAO() {
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see com.crossover.techtrial.dao.IPersonDAO#getTop5()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Person> getTop5() {
		String hql = "SELECT p FROM Person as p JOIN Ride as r ON p.id = r.id ORDER BY r.distance DESC";
		Object o = entityManager.createQuery(hql).setMaxResults(5).getResultList();
		return (List<Person>) o;
	}

}
