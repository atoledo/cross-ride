/**
 * 
 */
package com.crossover.techtrial.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.crossover.techtrial.model.Person;
import com.crossover.techtrial.model.Ride;

/**
 * @author adalberto
 *
 */
@Transactional
@Repository
public class RideDAO implements IRideDAO {

	@PersistenceContext	
	private EntityManager entityManager;
	
	/**
	 * 
	 */
	public RideDAO() {
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see com.crossover.techtrial.dao.IRideDAO#findByDriverId(java.lang.Long)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Ride> findByDriverId(Long driverId) {
		String hql = "SELECT r FROM Ride as r WHERE r.driver.id = " + driverId + " ORDER BY r.id";
		Object o = entityManager.createQuery(hql).getResultList();
		return (List<Ride>) o;
	}

}
