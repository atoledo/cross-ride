/**
 * 
 */
package com.crossover.techtrial.dao;

import java.util.List;

import com.crossover.techtrial.model.Person;

/**
 * @author adalberto
 *
 */
public interface IPersonDAO {
	List<Person> getTop5();
}
