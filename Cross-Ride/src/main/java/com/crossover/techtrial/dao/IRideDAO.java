/**
 * 
 */
package com.crossover.techtrial.dao;

import java.util.List;

import com.crossover.techtrial.model.Ride;

/**
 * @author adalberto
 *
 */
public interface IRideDAO {

	List<Ride> findByDriverId(Long driverId);

}
