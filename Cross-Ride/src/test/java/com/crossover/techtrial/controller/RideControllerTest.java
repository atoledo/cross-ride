/**
 * 
 */
package com.crossover.techtrial.controller;

import java.util.LinkedHashMap;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.crossover.techtrial.model.Person;
import com.crossover.techtrial.model.Ride;
import com.crossover.techtrial.repositories.PersonRepository;
import com.crossover.techtrial.repositories.RideRepository;

/**
 * @author adalberto
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class RideControllerTest {

	MockMvc mockMvc;
	  
	  @Mock
	  private RideController rideController;
	  
	  @Autowired
	  private TestRestTemplate template;
	  
	  @Autowired
	  RideRepository rideRepository;
	  
	  @Before
	  public void setup() throws Exception {
	    mockMvc = MockMvcBuilders.standaloneSetup(rideController).build();
	  }
	  
	  @Test
	  public void sendDataTest() throws Exception {
		  boolean driver = true; //to set if you want to add a driver or rider

		  //Get Persons From Database
		  @SuppressWarnings("rawtypes")
		ResponseEntity<List> responsePerson = template.getForEntity(
			        "/api/person", List.class);
		  
		  Person person = new Person();
		  person.setId(0L);
		  if(responsePerson.getBody() != null && responsePerson.getBody().size() > 0) {
			  @SuppressWarnings("unchecked")
			LinkedHashMap<String, Object> object = (LinkedHashMap<String, Object>) responsePerson.getBody().get(0);
			  person.setId(Long.parseLong(object.get("id").toString())); //set person the id gotten from DB
		  }
		  
	    HttpEntity<Object> ride = getHttpEntity(
	        "{\"" + (driver ? "driver" : "rider") + "\": {\"id\": \"" + person.getId() + "\"}," 
	            + " \"startTime\": \"2018-08-08T12:12:12\",\"endTime\":\"2018-08-08T12:12:13\","
	            + " \"distance\": \"2\"}");
	    ResponseEntity<Ride> response = template.postForEntity(
	        "/api/ride", ride, Ride.class); //call createNewRide
	    
	    if(response.getBody() != null && response.getBody().getId() != null) {
	    	//Delete this ride
	    	rideRepository.deleteById(response.getBody().getId());
	    	//verify if the driver/rider inserted is the same as the one gotten from DB 
	    	Assert.assertEquals((driver ? response.getBody().getDriver().getId() : response.getBody().getRider().getId()), person.getId());
	    }

	    Assert.assertEquals(200,response.getStatusCode().value()); //verify is it was an ok response
	  }

	  private HttpEntity<Object> getHttpEntity(Object body) {
	    HttpHeaders headers = new HttpHeaders();
	    headers.setContentType(MediaType.APPLICATION_JSON);
	    return new HttpEntity<Object>(body, headers);
	  }
	  
}
